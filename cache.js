function cacheFunction(cb) {
    let cache={}
    function invoke(arguments)
    {
        if (arguments in cache)
        {
            return cache(arguments)
                }
                else{
                    cache[arguments]=cb(arguments);
                    return cache[arguments];

                }
             }
              return invoke;

    }
    module.exports=cacheFunction