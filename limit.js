function limitFunctionCallCount(cb, n) {
  
    function invoke() {
        
          while (n > 0) {
            cb();
            n--;
          }
          return 'null'
        }
        return {invoke}
        
      };
      module.exports=limitFunctionCallCount